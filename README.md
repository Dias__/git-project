# Git Final Project

## Table of content

- [Table of content](#table-of-content)
- [Description](#description)
- [Requirements](#requirements)
- [Oral presentation](#oral-presentation)
- [Due date](#due-date)

## Description

Like the previous TP, the goal is still to make you practice with Git as a team. But this time, in a real project

## Requirements

- Start / Take a project (TBD)
- Setup and work on the repo following the Git Flow method:
  - Initialization
  - Branches (master, develop, release, ...)
  - Correct merges on master and develop, all branches up-to-date at the end
  - Tags with descriptions
- Write issues of what you plan to implement and assign yourselves to the issues when you work on it (for work you’ll do in this project and also future work that’ll not be done).
- Sign your commits.
- Write proper commit messages and merge requests describing the feature implemented
- Protect project and important branches
- Expected features:
  - Working project (as specified in the specs TBD)
  - Linter, imported from <https://gitlab.com/reggermont-teaching/git-project> as a submodule
  - Tests (any)
  - CI to run the linter / tests on GitHub / GitLab
  - Git hooks, at minimum a pre-push hook to run automatically the linter
  - Implement the `review_stats.sh` script saw in the Course 4, Slide 12

## Oral presentation

- 10 minutes for the presentation. Here are content suggestions
  - Presentation of your work
  - How you worked as a team with Git and difficulties encountered
  - GitHub / GitLab repos and implementations made (CI, Hooks, ...)
  - Statistics from `review_stats.sh`
  - Difficulties encountered
  - Demo
- 10 minutes for the questions

## Due date

Monday, 26th of July at 11:59 PM (means last commit tolerated)
